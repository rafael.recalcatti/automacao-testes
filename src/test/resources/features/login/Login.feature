#language:pt

@login
Funcionalidade: Login na API de Super App
  Desejamos realizar login

  @sucesso
  Cenario: Login com credenciais validas e sucesso
    Dado que estejam configurados os dados validos para o endpoint login
    Quando enviamos a requisicao ao endpoint login
    Entao o retorno recebido do endpoint login contem o token jwt

  @insucesso
  Cenario: Login com credenciais invalidas e insucesso
    Dado que estejam configurados os dados invalidos para o endpoint login
    Quando enviamos a requisicao ao endpoint login
    Entao o retorno recebido do endpoint login nao contem o token jwt
    
#  @wip
#  Cenario: Login com usuario correto e senha invalida
#  	Dado que estejam configurados o usuario correto e senha invalida para requicao ao endpoint login_hml
#  	Quando enviamos a requisicao ao endpoint login_hml
#  	Entao o retorno recebido do endpoint login_hml indica o erro XX
#
#  @wip
#  Cenario: Login com usuario invalido e senha valida
#  	Dado que estejam configurados o usuario invalido e senha valida para requicao ao endpoint login_hml
#  	Quando enviamos a requisicao ao endpoint login_hml
#  	Entao o retorno recebido do endpoint login_hml indica o erro YY
#
#  @wip
#  Cenario: Login com usuario invalido e senha invalida
#  	Dado que estejam configurados o usuario invalido e senha invalida para requicao ao endpoint login_hml
#  	Quando enviamos a requisicao ao endpoint login_hml
#  	Entao o retorno recebido do endpoint login_hml indica o erro ZZ
#
#  @wip
#  Cenario: Login com usuario em branco e senha em branco
#  	Dado que estejam configurados o usuario em branco e senha em branco para requicao ao endpoint login_hml
#  	Quando enviamos a requisicao ao endpoint login_hml
#  	Entao o retorno recebido do endpoint login_hml indica o erro WW