#language:pt
# encoding UTF-8

@cartoes yutyjutyjtyj
Funcionalidade: Acessar dados de cartoes
  Contexto: Obter token de autenticacao
    Dado que estejam configurados os dados validos para o endpoint login
    Quando enviamos a requisicao ao endpoint login
    Entao o retorno recebido do endpoint login contem o token jwt

  @cartao_valido_extrato
  Cenario: Obter extrato de cartao valido
    Dado que estejam configurados os dados validos para o endpoint cartao_extrato
    Quando enviamos a requisicao ao endpoint cartao_extrato
    Entao o retorno recebido do endpoint cartao contem o extrato do cartao consultado

  @cartao_invalido_extrato
  Cenario: Obter extrato de cartao invalido
    Dado que estejam configurados os dados ivalidos para o endpoint cartao_extrato
    Quando enviamos a requisicao ao endpoint cartao_extrato
    Entao o retorno recebido do endpoint cartao nao contem os dados do mesmo