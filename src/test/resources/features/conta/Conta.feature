#language:pt

@conta
Funcionalidade: Realizar operacoes com conta na API de Super App
  Desejamos realizar operacoes com conta

  Contexto: Obter token de autenticacao
    Dado que estejam configurados os dados validos para o endpoint login
    Quando enviamos a requisicao ao endpoint login
    Entao o retorno recebido do endpoint login contem o token jwt

  @comprovantes
  Cenario: Obter ultimos comprovantes de todo tipo
    Dado que enviamos a requisicao de comprovantes para o endpoint receipts_query
    Entao o retorno recebido do endpoint contera a lista de comprovantes valida