package br.com.inmetrics.qts.api;

import com.aventstack.extentreports.testng.listener.ExtentITestListenerClassAdapter;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.CucumberFeatureWrapper;
import cucumber.api.testng.TestNGCucumberRunner;
import org.testng.annotations.AfterClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@CucumberOptions(
        features = {"classpath:features"},
        tags = {"@conta, @cartoes"},
        plugin = {"json:target/cucumber-reports/cucumberTestReport.json"},
        glue = {"br.com.inmetrics.qts.api.steps"},
        monochrome = true,
        dryRun = false
)

@Listeners(ExtentITestListenerClassAdapter.class)
public class RunTest {

    private TestNGCucumberRunner testNGCucumberRunner = new TestNGCucumberRunner(this.getClass());

    @Test(groups = "cucumber", description = "Runs Cucumber Feature", dataProvider = "features")
    public void features(CucumberFeatureWrapper cucumberFeature) {
        testNGCucumberRunner.runCucumber(cucumberFeature.getCucumberFeature());
    }

    @DataProvider
    public Object[][] features() {
        return testNGCucumberRunner.provideFeatures();
    }
	
	@AfterClass(alwaysRun = true)
    public void tearDownClass() throws Exception {
        testNGCucumberRunner.finish();
    }
}
