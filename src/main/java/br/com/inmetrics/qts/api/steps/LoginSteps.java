package br.com.inmetrics.qts.api.steps;

import br.com.inmetrics.qts.api.dto.DataBodyDto;
import br.com.inmetrics.qts.api.dto.LoginDto;
import br.com.inmetrics.qts.api.factory.EncryptDataFactory;
import br.com.inmetrics.qts.api.factory.LoginDataFactory;
import br.com.inmetrics.qts.api.specification.RequestSpecificationBuilder;
import br.com.inmetrics.qts.api.support.utils.Specs;
import br.com.inmetrics.qts.api.support.utils.Token;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;

public class LoginSteps {

    private final RequestSpecification requestSpecification = Specs.getRequestSpec(RequestSpecificationBuilder.SpecType.LOGIN);
    private Response response;
    private final String path = "/login";
    private DataBodyDto dataBodyDto;

    @Dado("^que estejam configurados os dados validos para o endpoint login$")
    public void queEstejamConfiguradosOsDadosParaOEndpointLogin() throws Exception {
        LoginDto loginDto = new LoginDataFactory().buildUser();
        dataBodyDto = DataBodyDto.builder().data(
                new EncryptDataFactory().encryptData(loginDto.toString())).build();
    }

    @Quando("^enviamos a requisicao ao endpoint login$")
    public void enviamosARequisicaoAoEndpointLogin() throws Exception {
        response = RestAssured.given().
                spec(requestSpecification).
                when().
                body(dataBodyDto).
                post(path).
                then().extract().response();
    }

    @Entao("^o retorno recebido do endpoint login contem o token jwt$")
    public void oRetornoRecebidoDoEndpointLoginContemOTokenJwt() throws Exception {
        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertNotNull(response.body().jsonPath().getString("jwtToken"));
        String token = response.body().jsonPath().getString("jwtToken");
        Token.getInstance().setJwtToken(token);
    }

    @Dado("^que estejam configurados os dados invalidos para o endpoint login$")
    public void queEstejamConfiguradosOsDadosInvalidosParaOEndpointLogin() throws Exception {
        LoginDto loginDto = new LoginDataFactory().buildUser();
        dataBodyDto = DataBodyDto.builder().data(
                new EncryptDataFactory().encryptData1(loginDto.toString())).build();
    }

    @Entao("^o retorno recebido do endpoint login nao contem o token jwt$")
    public void oRetornoRecebidoDoEndpointLoginNaoContemOTokenJwt() {
        Assert.assertEquals(response.statusCode(), 400);
        Assert.assertNull(response.body().jsonPath().getString("jwtToken"));
    }

}
