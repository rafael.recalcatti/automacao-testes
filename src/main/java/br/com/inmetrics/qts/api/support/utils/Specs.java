package br.com.inmetrics.qts.api.support.utils;

import br.com.inmetrics.qts.api.specification.RequestSpecificationBuilder;
import io.restassured.specification.RequestSpecification;
import lombok.Getter;
import br.com.inmetrics.qts.api.specification.RequestSpecificationBuilder.*;

public class Specs {

    private static RequestSpecificationBuilder requestSpecificationBuilder = new RequestSpecificationBuilder();

    private Specs() {

    }

    public static RequestSpecification getRequestSpec(SpecType specType) {
        return requestSpecificationBuilder.build(specType);
    }

}


