package br.com.inmetrics.qts.api.support.constants;

import io.restassured.http.Header;

public class ServiceConstants {

	// Header
	public static final Header CONTENT_TYPE_JSON = new Header("Content-Type", "application/json");
	public static final Header FORM_DATA = new Header("Content-Type", "multipart/form-data");
	public static final Header BROWSER = new Header("browser", "RegressionTest");
	public static final Header PLAFATORM = new Header("platform", "RegressionTest");
	public static final Header ACCEPT_UTF8 = new Header("Accept-Encoding", "UTF-8");
	public static final Header IFOOD_SESSION_ID = new Header("X-Ifood-Session-Id", "Test");
	public static final Header IFOOD_CLIENT_IP = new Header("X-iFood-Client-Ip", "Test");
	public static final Header REMOTE_ADDR = new Header("Remote_Addr", "Test");
	public static final Header ACCEPT_LANGUAGE = new Header("Accept-Language", "application/json");
	public static final Header HTTP_X_FORWARDED_FOR = new Header("HTTP_X_FORWARDED_FOR", "Teste");
	public static final Header ACCESS_KEY = new Header("access_key", "123123");
	public static final Header SECRET_KEY = new Header("secret_key", "321321");
	// tribanco HML
	public static final Header WRAPPER_HML = new Header("wrapper", "cU49JBpq/WKT6fyLrBKxtPQr5WVLFwbIphUJ36WzDrYz9SUJBV278uGcvsbEdgnSPWtCd2vTWur9KaDeKbpTdhVlaM+K/j+wE9utlWTI6cJu0zEMSOBpod+jf+cOqqb4pvqLBjq8ZWw3Vi30wXhE9iENvsOp1bus4eY+VDxGcT31Y7iaaGyJW+igULn8zjOLJZUs8pTGYpBhigdMOjDkL7a5T4RlqfVGOGdb/LOdnxQ7VnnvzlGMU6deNXCbvIiv1KnXh0V5IRP0i2rqkVcp3+NDqIhY7iBV2q5SdimuCp4YcGAEG0pEOSSl3p+uk/GWUXeY4lwLBg/32tJTdV+c4g==:XFj2MCGW0rAPaJVD3f7gjLO611Jc5ZenKaiIY7Z/1Blo00B0z7l8I6zg7+i4KNjSUn6DkHDvtqOXGtWJv285gVPKd8zyMkM3YqpiJ4gYeL2ygFjEP84KNNK0DLVY19Q056GNn/idg52Spbaos7V2MD6EBYb8BCiSQJ//47+VQi9d8kOuZwPgXb1PKkMgIII7Cf6xxW0gWTtRFjLKWKD2Gw+GGeZMLMNs/ylJRgpp8DxGnIUpH4YBbG5u6Cu3b/GxqL7PTGKMGXH+QN92DmcT/Yqps3cHbfUoefVmC7et3g+uOyTf/1vEIbbD+p2BYJB8vgrTJzmQenHSamyMB/eLCg==");
	public static final Header DEVICE_HML = new Header("x-device-info", "eyJjYXJyaWVySXBBZGRyZXNzIjoiMTkyLjE2OC4xMDAuNDMiLCJkZXZpY2VBcHBWZXJzaW9uIjoiNC4wLjIwIiwiZGV2aWNlTW9kZWwiOiJTTS1BMDEzTSIsImRldmljZVRpbWVab25lIjoiR01ULTAzOjAwIiwiZGV2aWNlVVVJRCI6IlNNLUEwMTNNLTMzMTI1Njc4NzQ5IiwiZHB2SWQiOiI1ODM4YjMxMC0yMWMyLTQ4OTEtYmY0MS03ODU0MTc3NzM2ZjAiLCJsYXRpdHVkZSI6LTE4LjkyMzk2NDYsImxvbmdpdHVkZSI6LTQ4LjI1NjQ3NTIsInRheElkIjoiMzMxMjU2Nzg3NDkifQ==");
	public static final Header CLIENT_ID = new Header("client_id", "a030ae5c-294a-3e2a-91bd-dc5dd595ea99");
	//public static final Header AUTHORIZATION = new Header("Authorization", "");
}
