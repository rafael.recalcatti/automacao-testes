package br.com.inmetrics.qts.api.support.utils;

import lombok.Getter;
import lombok.Setter;

public class Token {

    @Getter
    @Setter
    private String jwtToken;
    private static Token instance;

    private Token(){
    }

    public static Token getInstance() {
        if(instance == null){
            instance = new Token();
        }
        return instance;
    }
}
