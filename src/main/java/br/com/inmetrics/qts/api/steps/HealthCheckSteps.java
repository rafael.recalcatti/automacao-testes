package br.com.inmetrics.qts.api.steps;

import br.com.inmetrics.qts.api.specification.RequestSpecificationBuilder;
import br.com.inmetrics.qts.api.support.utils.Specs;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;

public class HealthCheckSteps {

    private final RequestSpecification requestSpecification = Specs.getRequestSpec(RequestSpecificationBuilder.SpecType.DEFAULT);
    private Response response;
    private final String path = "/actuator/health";

    @Dado("^que eu efetuo uma requisicao para o end point actuator$")
    public void queEuEfetuoUmaRequisicaoParaOEndPointActuator() throws Exception {

        response = RestAssured.given().
                spec(requestSpecification).
                when().
                get(path).
                then().extract().response();
    }

    @Entao("^eu recebo o status up do servico$")
    public void euReceboOStatusUpDoServico() {
        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertEquals(response.body().jsonPath().getString("status"), "UP");
    }
}
