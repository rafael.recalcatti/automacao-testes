package br.com.inmetrics.qts.api.specification;

import br.com.inmetrics.qts.api.support.utils.ConfigConstants;
import br.com.inmetrics.qts.api.support.utils.Token;
import br.com.inmetrics.qts.api.support.utils.YamlHelper;
import br.com.inmetrics.qts.api.support.utils.Utils;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.specification.RequestSpecification;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;

@Slf4j
public class RequestSpecificationBuilder {


    public enum SpecType {

        DEFAULT(0),
        AUTHENTICATED(1),
        LOGIN(2);

        Integer valor;

        SpecType(Integer valor) {
            this.valor = valor;
        }

        public Integer value() {
            return valor;
        }
    }

    public RequestSpecification build(SpecType specType) {

        RequestSpecBuilder requestSpecBuilder = null;
        try {

            switch (specType) {

                case DEFAULT:
                    requestSpecBuilder = requestSpecBuilderDefault();
                    break;
                case AUTHENTICATED:
                    requestSpecBuilder = requestSpecBuilderAuth();
                    break;
                case LOGIN:
                    requestSpecBuilder = requestSpecBuilderLogin();
                    break;
            }

        } catch (Exception e) {
            log.error(e.getMessage());
        }

        return requestSpecBuilder.build();
    }

    private RequestSpecBuilder requestSpecBuilderDefault() throws Exception {

        YamlHelper yml = new YamlHelper(ConfigConstants.PATH_URLS);
        String url = yml.getAttribute(Utils.getEnvironment(), "url").toString();

        RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
//        requestSpecBuilder.setConfig(new RestAssuredConfig()
//                .sslConfig(new SSLConfig().relaxedHTTPSValidation())
//                .logConfig(LogConfig.logConfig().defaultStream(new PrintStreamExtent(System.out))));
        requestSpecBuilder.setRelaxedHTTPSValidation();
        requestSpecBuilder.log(LogDetail.ALL);
        requestSpecBuilder.addFilter(new ResponseLoggingFilter());
        requestSpecBuilder.addHeader("Content-Type", "application/json");
        requestSpecBuilder.addHeader("client_id", "a030ae5c-294a-3e2a-91bd-dc5dd595ea99");
        requestSpecBuilder.setBaseUri(url);
        requestSpecBuilder.setBasePath("/");

        return requestSpecBuilder;

    }

    private RequestSpecBuilder requestSpecBuilderLogin() throws Exception {

        YamlHelper yml = new YamlHelper(ConfigConstants.PATH_URLS);
        String url = yml.getAttribute(Utils.getEnvironment(), "url").toString();

        RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
//        requestSpecBuilder.setConfig(new RestAssuredConfig()
//                .sslConfig(new SSLConfig().relaxedHTTPSValidation())
//                .logConfig(LogConfig.logConfig().defaultStream(new PrintStreamExtent(System.out))));
        requestSpecBuilder.setRelaxedHTTPSValidation();
        requestSpecBuilder.log(LogDetail.ALL);
        requestSpecBuilder.addFilter(new ResponseLoggingFilter());
        requestSpecBuilder.addHeader("Content-Type", "application/json");
        requestSpecBuilder.addHeader("client_id", "a030ae5c-294a-3e2a-91bd-dc5dd595ea99");
        requestSpecBuilder.addHeader("wrapper", "cU49JBpq/WKT6fyLrBKxtPQr5WVLFwbIphUJ36WzDrYz9SUJBV278uGcvsbEdgnSPWtCd2vTWur9KaDeKbpTdhVlaM+K/j+wE9utlWTI6cJu0zEMSOBpod+jf+cOqqb4pvqLBjq8ZWw3Vi30wXhE9iENvsOp1bus4eY+VDxGcT31Y7iaaGyJW+igULn8zjOLJZUs8pTGYpBhigdMOjDkL7a5T4RlqfVGOGdb/LOdnxQ7VnnvzlGMU6deNXCbvIiv1KnXh0V5IRP0i2rqkVcp3+NDqIhY7iBV2q5SdimuCp4YcGAEG0pEOSSl3p+uk/GWUXeY4lwLBg/32tJTdV+c4g==:XFj2MCGW0rAPaJVD3f7gjLO611Jc5ZenKaiIY7Z/1Blo00B0z7l8I6zg7+i4KNjSUn6DkHDvtqOXGtWJv285gVPKd8zyMkM3YqpiJ4gYeL2ygFjEP84KNNK0DLVY19Q056GNn/idg52Spbaos7V2MD6EBYb8BCiSQJ//47+VQi9d8kOuZwPgXb1PKkMgIII7Cf6xxW0gWTtRFjLKWKD2Gw+GGeZMLMNs/ylJRgpp8DxGnIUpH4YBbG5u6Cu3b/GxqL7PTGKMGXH+QN92DmcT/Yqps3cHbfUoefVmC7et3g+uOyTf/1vEIbbD+p2BYJB8vgrTJzmQenHSamyMB/eLCg==");
        requestSpecBuilder.addHeader("x-device-info", "eyJjYXJyaWVySXBBZGRyZXNzIjoiMTkyLjE2OC4xMDAuNDMiLCJkZXZpY2VBcHBWZXJzaW9uIjoiNC4wLjIwIiwiZGV2aWNlTW9kZWwiOiJTTS1BMDEzTSIsImRldmljZVRpbWVab25lIjoiR01ULTAzOjAwIiwiZGV2aWNlVVVJRCI6IlNNLUEwMTNNLTMzMTI1Njc4NzQ5IiwiZHB2SWQiOiI1ODM4YjMxMC0yMWMyLTQ4OTEtYmY0MS03ODU0MTc3NzM2ZjAiLCJsYXRpdHVkZSI6LTE4LjkyMzk2NDYsImxvbmdpdHVkZSI6LTQ4LjI1NjQ3NTIsInRheElkIjoiMzMxMjU2Nzg3NDkifQ==");
        requestSpecBuilder.setBaseUri(url);
        requestSpecBuilder.setBasePath("/");

        return requestSpecBuilder;

    }

    private RequestSpecBuilder requestSpecBuilderAuth() throws Exception {

        YamlHelper yml = new YamlHelper(ConfigConstants.PATH_URLS);
        String url = yml.getAttribute(Utils.getEnvironment(), "url").toString();
        String token = Objects.nonNull(Token.getInstance().getJwtToken()) ? Token.getInstance().getJwtToken() : null;
        RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
//        requestSpecBuilder.setConfig(new RestAssuredConfig()
//                .sslConfig(new SSLConfig().relaxedHTTPSValidation())
//                .logConfig(LogConfig.logConfig().defaultStream(new PrintStreamExtent(System.out))));
        requestSpecBuilder.setRelaxedHTTPSValidation();
        requestSpecBuilder.log(LogDetail.ALL);
        requestSpecBuilder.addFilter(new ResponseLoggingFilter());
        requestSpecBuilder.addHeader("Content-Type", "application/json");
        requestSpecBuilder.addHeader("client_id", "a030ae5c-294a-3e2a-91bd-dc5dd595ea99");
        requestSpecBuilder.addHeader("wrapper", "cU49JBpq/WKT6fyLrBKxtPQr5WVLFwbIphUJ36WzDrYz9SUJBV278uGcvsbEdgnSPWtCd2vTWur9KaDeKbpTdhVlaM+K/j+wE9utlWTI6cJu0zEMSOBpod+jf+cOqqb4pvqLBjq8ZWw3Vi30wXhE9iENvsOp1bus4eY+VDxGcT31Y7iaaGyJW+igULn8zjOLJZUs8pTGYpBhigdMOjDkL7a5T4RlqfVGOGdb/LOdnxQ7VnnvzlGMU6deNXCbvIiv1KnXh0V5IRP0i2rqkVcp3+NDqIhY7iBV2q5SdimuCp4YcGAEG0pEOSSl3p+uk/GWUXeY4lwLBg/32tJTdV+c4g==:XFj2MCGW0rAPaJVD3f7gjLO611Jc5ZenKaiIY7Z/1Blo00B0z7l8I6zg7+i4KNjSUn6DkHDvtqOXGtWJv285gVPKd8zyMkM3YqpiJ4gYeL2ygFjEP84KNNK0DLVY19Q056GNn/idg52Spbaos7V2MD6EBYb8BCiSQJ//47+VQi9d8kOuZwPgXb1PKkMgIII7Cf6xxW0gWTtRFjLKWKD2Gw+GGeZMLMNs/ylJRgpp8DxGnIUpH4YBbG5u6Cu3b/GxqL7PTGKMGXH+QN92DmcT/Yqps3cHbfUoefVmC7et3g+uOyTf/1vEIbbD+p2BYJB8vgrTJzmQenHSamyMB/eLCg==");
        requestSpecBuilder.addHeader("x-device-info", "eyJjYXJyaWVySXBBZGRyZXNzIjoiMTkyLjE2OC4xMDAuNDMiLCJkZXZpY2VBcHBWZXJzaW9uIjoiNC4wLjIwIiwiZGV2aWNlTW9kZWwiOiJTTS1BMDEzTSIsImRldmljZVRpbWVab25lIjoiR01ULTAzOjAwIiwiZGV2aWNlVVVJRCI6IlNNLUEwMTNNLTMzMTI1Njc4NzQ5IiwiZHB2SWQiOiI1ODM4YjMxMC0yMWMyLTQ4OTEtYmY0MS03ODU0MTc3NzM2ZjAiLCJsYXRpdHVkZSI6LTE4LjkyMzk2NDYsImxvbmdpdHVkZSI6LTQ4LjI1NjQ3NTIsInRheElkIjoiMzMxMjU2Nzg3NDkifQ==");
        requestSpecBuilder.setBaseUri(url);
        requestSpecBuilder.setBasePath("/");
        requestSpecBuilder.addHeader("Authorization", "Bearer " + token);

        return requestSpecBuilder;

    }
}
