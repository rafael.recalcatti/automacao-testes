package br.com.inmetrics.qts.api.dto;

import lombok.*;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DataBodyDto {

    private String data;

}
