package br.com.inmetrics.qts.api;


import br.com.inmetrics.qts.api.specification.RequestSpecificationBuilder;
import br.com.inmetrics.qts.api.support.utils.Utils;
import cucumber.api.Scenario;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;


@Slf4j
@Getter
public class Hooks {

	protected RequestSpecificationBuilder requestSpecBuilder = new RequestSpecificationBuilder();
	
	@BeforeTest
	public void setUp(Scenario scenario) throws IllegalAccessException {
		log.info("*** Iniciando - " + scenario.getId() + " " + scenario.getName() + " ***");
		log.info("*** Ambiente - " + Utils.getEnvironment() + " ***");
		log.info("*** Carregando os specs ***");
	}

	@AfterTest
	public void tearDown(Scenario scenario) {
		log.info("*** Encerrando - " + scenario.getId() + " " + scenario.getName() + " ***");
		log.info("*** Status - " + scenario.getStatus() + " ***");
	}
}
