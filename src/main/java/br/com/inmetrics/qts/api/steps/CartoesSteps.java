package br.com.inmetrics.qts.api.steps;

import br.com.inmetrics.qts.api.dto.DataBodyDto;
import br.com.inmetrics.qts.api.dto.LoginDto;
import br.com.inmetrics.qts.api.factory.LoginDataFactory;
import br.com.inmetrics.qts.api.specification.RequestSpecificationBuilder;
import br.com.inmetrics.qts.api.support.utils.Specs;
import cucumber.api.java.pt.*;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;
import org.hamcrest.core.IsNull;

import static org.hamcrest.CoreMatchers.is;

public class CartoesSteps {

    private final RequestSpecification requestSpecification = Specs.getRequestSpec(RequestSpecificationBuilder.SpecType.AUTHENTICATED);
    private Response response;
    private final String path = "/card/statements";
    private DataBodyDto dataBodyDto;
    private String cardLastDigits = null;

    @Dado("^que estejam configurados os dados validos para o endpoint cartao_extrato$")
    public void queEstejamConfiguradosOsDadosValidosParaOEndpointCartao_extrato() {

    }

    @Quando("^enviamos a requisicao ao endpoint cartao_extrato")
    public void enviamosARequisicaoAoEndpointCartao_extrato() throws Exception {
        response = RestAssured.given().
                spec(requestSpecification).
                queryParam("cardLastDigits", cardLastDigits).
                when().
                get(path).
                then().extract().response();
    }

    @Entao("^o retorno recebido do endpoint cartao contem o extrato do cartao consultado$")
    public void oRetornoRecebidoDoEndpointCartaoContemOExtratoDoCartaoConsultado() {

    }

    @Dado("^que estejam configurados os dados ivalidos para o endpoint cartao_extrato$")
    public void queEstejamConfiguradosOsDadosIvalidosParaOEndpointCartao_extrato() {
        cardLastDigits = "0119";
    }

    @Entao("^o retorno recebido do endpoint cartao nao contem os dados do mesmo$")
    public void oRetornoRecebidoDoEndpointCartaoNaoContemOsDadosDoMesmo() {
        Assert.assertEquals(response.getStatusCode(), 400);
        Assert.assertFalse(response.body().jsonPath().getString("timestamp").isEmpty());
        Assert.assertEquals(response.body().jsonPath().getString("status"), "400");
    }

}
