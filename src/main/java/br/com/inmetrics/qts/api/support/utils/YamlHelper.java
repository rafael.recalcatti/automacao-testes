package br.com.inmetrics.qts.api.support.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Map;

import org.yaml.snakeyaml.Yaml;

/**
 * Inmetrics<BR>
 *
 * Classe que permite a leitura de massa de dados de arquivos .yaml<BR>
 *
 * @since 29 de nov de 2017 08:57:34
 * @author Renato Vieira<BR>
 *         Inmetrics<BR>
 * 
 *         toolset
 */

public class YamlHelper {

	private String path;

	public YamlHelper(String path) {
		this.path = path;
	}

	/**
	 * Inmetrics<BR>
	 * 
	 * Realiza a leitura de arquivos .yaml e retorna um determinado valor<BR>
	 *
	 * @since 24 de mai de 2017 13:12:19
	 * @author Renato Vieira<BR>
	 * @throws Exception
	 */
	public Object getAttribute(String... param) throws Exception {

		File file = new File(this.path);
		InputStream input = new FileInputStream(file);

		Map<?, ?> mapAux = new Yaml().load(input);

		if (mapAux == null) {
			throw new Exception(String.format("A massa de dados e/ou configuração não foi localizada no arquivo %s",
					file.getName()));
		}

		int cont;

		for (cont = 0; cont < (param.length - 1); cont++) {
			mapAux = (Map<?, ?>) mapAux.get(param[cont]);	
		}
		
		return mapAux.get(param[cont]);
	}
}
