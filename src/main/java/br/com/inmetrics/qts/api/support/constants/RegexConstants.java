package br.com.inmetrics.qts.api.support.constants;

/**
 *
 * Feature INMSRT-164 - [Débito Técnico] Ajuste da lógica de validação de
 * mensagens dinâmicas para expressão regular<BR>
 *
 * @since 25 de set de 2020 10:09:09
 * @author Renato Vieira<BR>
 *         Inmetrics<BR>
 * 
 *         inmetrics-api-regressivo
 */
public class RegexConstants {

	// Authentication
	public static final String AUTHENTICATIONS_UUID = "^[\\\\\\w\\sÀ-ú]{8}(-)[\\\\\\w\\sÀ-ú]{4}(-)[\\\\\\w\\sÀ-ú]{4}(-)[\\\\\\w\\sÀ-ú]{4}(-)[\\\\\\w\\sÀ-ú]{12}+$";
}
