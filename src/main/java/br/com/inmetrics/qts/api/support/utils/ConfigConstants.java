package br.com.inmetrics.qts.api.support.utils;

public class ConfigConstants {
	
	public static final String PATH_URLS = "./src/main/resources/data/endpoints/endpoint.yaml";
	public static final String PATH_DATA = "./src/main/resources/data/dev.yaml";
	
	public static final long TIMEOUT = 15;
	public static final boolean LOGGER = true;
	public static final boolean URL_ENCONDING = false;

}
