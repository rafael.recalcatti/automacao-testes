package br.com.inmetrics.qts.api.support.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.util.*;
import java.util.regex.Pattern;

import cucumber.api.DataTable;
import cucumber.api.Scenario;
import lombok.extern.slf4j.Slf4j;

/**
 * Inmetrics<BR>
 * <p>
 * Classe de métodos utilitários<BR>
 *
 * @author Junior Sbrissa<BR>
 * Inmetrics<BR>
 * <p>
 * api-cucumber-allure
 */

@Slf4j
public class Utils {

    private static String region = "";

    private static String environment = "dev";

    private static Map<String, String> dao = new HashMap<String, String>();

    public static String getProperties(String file, String propertie) {
        Properties propertiesProperties = new Properties();
        FileInputStream fileProperties;
        try {
            fileProperties = new FileInputStream("./src/test/resources/properties/" + file);
            propertiesProperties.setProperty("path", System.getProperty("user.home"));
            InputStreamReader readerProperties = new InputStreamReader(fileProperties, "UTF-8");
            propertiesProperties.load(readerProperties);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return propertiesProperties.getProperty(propertie);
    }

    public static String encode(String content) throws Exception {
        return "orderJson=" + URLEncoder.encode(content, "UTF-8");
    }


    public static String getValueDataTable(DataTable data, String key) {
        String value = "";
        try {
            value = data.asMaps(String.class, String.class).get(0).get(key).toString();
        } catch (Exception e) {
            List<String> keyss = data.asLists(String.class).get(0);
            List<String> valuess = data.asLists(String.class).get(1);

            for (int i = 0; i < keyss.size(); i++) {
                if (keyss.get(i).equals(key)) {
                    value = valuess.get(i);
                }
            }
        }
        return value;
    }

    public static void setRegion(Scenario scenario) throws Exception {
        if (scenario.getSourceTagNames().contains("@latam")) {
            region = "latam";
        } else if (scenario.getSourceTagNames().contains("@br")) {
            region = "br";
        } else {
            throw new Exception("Não foi possível capturar a região");
        }
    }

    public static String getRegion() {
        return region;
    }

    public static String getEnvironment() {
        if (System.getProperty("environment") == null) {
            log.info("Está sendo utilizado o ambiente DEFAULT: " + environment);
            return environment;
        }

        environment = System.getProperty("environment");
        log.info("Está sendo utilizado o ambiente: " + environment);
        return environment;
    }

    public static String getRandom(int num) {
        return String.valueOf(new Random().nextInt(num));
    }

    public static long getTimestampNow() {
        return new Timestamp((System.currentTimeMillis() / 1000)).getTime();
    }

    public static long getTimestampPlusMin(int min) {
        Date dataDoUsuario = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(dataDoUsuario);
        c.add(Calendar.MINUTE, min);
        dataDoUsuario = c.getTime();
        return new Timestamp(dataDoUsuario.getTime() / 1000).getTime();
    }

    /**
     * <p>
     * INMSRT-164 - [Débito Técnico] Ajuste da lógica de validação de mensagens
     * dinâmicas para expressão regular<BR>
     *
     * @author Renato Vieira<BR>
     * @since 25 de set de 2020 12:49:42
     */
    public static boolean matcher(String regex, String input) {
        return Pattern.compile(regex).matcher(input).matches();
    }

    /**
     * Method performs waiting in seconds
     *
     * @author Icaro Silva<BR>
     * @since 29 de set de 2020 10:49:42
     */
    public static void wait(int seconds) throws Exception {
        try {
            Thread.sleep(seconds * 1000);
        } catch (Exception e) {
            throw new Exception("Couldn't wait" + e);
        }
    }

    /**
     * Add information Dao
     *
     * @author Icaro Silva<BR>
     * @since 29 de set de 2020 11:25:42
     */
    public static void put(String key, String value) {
        dao.put(key, value);
    }

    /**
     * Return information Dao
     *
     * @author Icaro Silva<BR>
     * @since 29 de set de 2020 11:49:42
     */
    public static String get(String key) {
        return dao.get(key);
    }
}
