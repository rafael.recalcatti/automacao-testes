package br.com.inmetrics.qts.api.support.constants;

public class PathConstants {
	
	public static final String PATH_JSON_BR_INMETRICS_WS_V3_V3_ORDERS_POST = "./src/main/resources/payloads/checkout/orders.json";
	public static final String PATH_JSON_MX_INMETRICS_WS_V3_V3_ORDERS_POST = "./src/main/resources/payloads/checkout/orders-mx.json";
	public static final String PATH_JSON_CAMPAIGN_PROMOTIONS_POST = "./src/main/resources/payloads/campaign_promotions/campaign.json";
	
	public static final String PATH_CSV_PARTICIPANTS = "./src/main/resources/files/campaign_promotions/participants/participants.csv";
	
}

