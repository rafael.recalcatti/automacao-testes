package br.com.inmetrics.qts.api.dto;

import lombok.*;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class LoginDto {

    private String user;
    private String pass;
}
