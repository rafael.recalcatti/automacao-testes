package br.com.inmetrics.qts.api.support.utils;

import br.com.inmetrics.qts.api.support.utils.ConfigConstants;
import br.com.inmetrics.qts.api.support.utils.YamlHelper;

public class DataSearch {

	static YamlHelper yml = new YamlHelper(ConfigConstants.PATH_DATA);
	
	public static String get(String... keys) throws Exception {
		return yml.getAttribute(keys).toString();
	}
}
