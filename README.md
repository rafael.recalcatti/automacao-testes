# Inmetrics Framework API

Inmetrics Framework API is a regression test automation project for API under the care of Inmetrics S/A.

##### Dependencies used:
 - [Rest-Assured](https://rest-assured.io/)
 - [Cucumber](https://cucumber.io/)
 - [jUnit](https://junit.org/junit4/)
 - [Maven](https://maven.apache.org/)
 - [Lombok](https://projectlombok.org/)
 - [Json](https://www.json.org/json-pt.html)
 
 ##### Configure and install:
 - Windows 7/8/8.1/10
 - VPN iFood
 - IDE of your choice:
   + Java (JDK 1.8)
   + Maven (Mvn 3.6)
   + Cucumber (4.8)
   + Lombok (1.18.8)
 
##### Run:
```sh
 mvn test -Dcucumber.options="--tags @tag" -Drp.enable=true -Drp.attributes=feature_region -Drp.description="Description" -Drp.launch=name_launch -Denvironment="env"
```

####### Example run
```sh
mvn test -Dcucumber.options="--tags @checkout --tags @br" -Drp.enable=true -Drp.attributes=checkout_br -Drp.description="RUN PUSH DEV" -Drp.launch=regression-test-dev -Denvironment="dev"
```

Any questions regarding this project, contact us through the Slack iFood channel: partner-inmetrics

Note: this document is being enriched
Teste Camargo